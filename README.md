# Document Scrapper

Simple web scrapper that looks for a given list of document types and image formats throughout a website and downloads them.

## Setup and demo

```bash
#!/bin/bash
git clone https://bitbucket.org/caathento/doc-scrapper.git
pip install virtualenv
virtualenv -p python2.7 env
source env/bin/activate
pip install -r requirements.txt
scrapy crawl scrapper -o output.json [-a <argument>=<value> ...]
```

It will download a bunch of minimalist wallpapers on your `docs/images` folder.

To customize the scrapper, take a look inside `scrapper/spiders/scrapper_spider.py`.

## Benchmarks

> Deprecated results since `7d1548b`

```bash
>>> scrapy crawl scrapper -o docs/fuengirola.json -a limit=1000 -a urls=http://www.fuengirola.es/portal_localweb/InitIndex.do -a domains=fuengirola.es

#    ================================
#    |       📊 Benchmarks 📊       |
#    ================================
#
#    ⏳ Start time:          13:07:57
#    ⌛️ End time:            13:07:59
#    ⏱ Delta time:     00:00:02.5757
#    🕷 Crawled pages:             33
#    📚 Scrapped items:            19

>>> scrapy crawl scrapper -o docs/madrid.json -a limit=1000 -a urls=https://www.madrid.es/portal/site/munimadrid -a domains=madrid.es

# Forbidden by robots.txt ...

>>> scrapy crawl scrapper -o docs/andalucia.json -a limit=1000 -a urls=http://www.juntadeandalucia.es/index.html -a domains=juntadeandalucia.es

#    ================================
#    |       📊 Benchmarks 📊       |
#    ================================
#
#    ⏳ Start time:          13:46:45
#    ⌛️ End time:            13:46:46
#    ⏱ Delta time:     00:00:01.5356
#    🕷 Crawled pages:            283
#    📚 Scrapped items:            31
```
