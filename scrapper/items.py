import scrapy


class DocumentItem(scrapy.Item):
    file_urls = scrapy.Field()
    files = scrapy.Field()

class ImageItem(scrapy.Item):
    image_urls = scrapy.Field()
    images = scrapy.Field()
