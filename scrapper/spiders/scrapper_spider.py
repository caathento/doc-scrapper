# -*- coding: utf-8 -*-

import time
import math
import scrapy
from scrapper.items import DocumentItem, ImageItem


# CLI: scrapy crawl scrapper -o output.json -a <argument>=<value> ...
# arguments:
#   - limit (number): amount of pages to crawl.
#   - urls (csv): list of pages to start crawling from.
#   - domains (csv): list of allowed domains to crawl through.
#   - docs (csv): list of document extensions to search for.
#     - default: pdf, docx, doc, odt, ott, xlsx, xls, ods, ots, pptx, ppt, odp, otp, odg, rtf, txt, csv
#   - imgs (csv): list of image extensions to search for.
#     - default: png, jpg, jpeg, gif, tiff, svg, raw
#
#   eg.
#     -a limit=100 -a urls=http://simpledesktops.com/browse/
#     -a domains=simpledesktops.com -a docs=pdf,docx
#     -a imgs=png,jpg,jpeg,gif,tiff
class ScrapperSpider(scrapy.Spider):
    # spider name
    name = 'scrapper'

    # domains the spider is allowd to crawl through
    allowed_domains = [
        'bitsavers.org',
        # 'madrid.es',
        # 'juntadeandalucia.es',
        # 'fuengirola.es',
    ]

    # urls to start crawling from
    urls_ = [
        'http://www.bitsavers.org/',
        # 'https://www.madrid.es/portal/site/munimadrid',
        # 'http://www.juntadeandalucia.es/index.html',
        # 'http://fuengirola.es/index.html',
    ]

    # doctypes & images to search for
    docs_ = [
        '.pdf', # portable document format
        '.docx', '.doc', '.odt', '.ott', # text (ms & open)
        '.xlsx', '.xls', '.ods', '.ots', # spreadsheets (ms & open)
        '.pptx', '.ppt', '.odp', '.otp', '.odg', # slides (ms & open)
        '.rtf', '.txt', '.csv', # plain text
    ]
    imgs_ = [
        '.png',
        '.jpg', '.jpeg',
        '.gif',
        '.tiff',
        '.svg',
        '.raw',
    ]

    # pages processed
    limit_ = None
    processed = 0

    # benchmarking
    bm_start_time = 0
    bm_end_time = 0
    bm_delta_time = 0
    bm_items = 0
    bm_pages = 0

    def fetch_args(self):
        """
        Iterates over CLI arguments (-a foo=bar) and sets
        up the correponding attributes.
        """
        limit = getattr(self, 'limit', None)
        if limit is not None:
            self.limit_ = limit
        urls = getattr(self, 'urls', None)
        if urls is not None:
            self.urls_ = urls.split(',')
        domains = getattr(self, 'domains', None)
        if domains is not None:
            self.allowed_domains = domains.split(',')
        docs = getattr(self, 'docs', None)
        if docs is not None:
            self.docs_ = docs.split(',')
        imgs = getattr(self, 'imgs', None)
        if imgs is not None:
            self.imgs_ = imgs.split(',')

        print('>>> param limit: %s <<<' % self.limit_)
        print('>>> param urls: %s <<<' % self.urls_)
        print('>>> param domains: %s <<<' % self.allowed_domains)
        print('>>> param docs: %s <<<' % self.docs_)
        print('>>> param imgs: %s <<<' % self.imgs_)

    def start_requests(self):
        """
        Called when the spider is about to beging crawling.
        """
        self.bm_start_time = time.time()
        self.fetch_args()
        for url in self.urls_:
            self.processed += 1
            self.bm_pages += 1
            yield scrapy.Request(url=url, callback=self.parse)

    def closed(self, reason):
        """
        Called when the spider has finished crawling.
        """
        self.bm_end_time = time.time()
        self.bm_delta_time = self.bm_end_time - self.bm_start_time
        dt_h = math.floor(self.bm_delta_time / 3600)
        dt_m = math.floor(self.bm_delta_time / 60)
        dt_s = self.bm_delta_time % 60

        print("""
    ================================
    |       📊 Benchmarks 📊       |
    ================================

    ⏳ Start time:     %(start_time)13s
    ⌛️ End time:       %(end_time)13s
    ⏱  Delta time:     %(delta_h)02d:%(delta_m)02d:%(delta_s)07.4f
    🕷  Crawled pages:  %(crawled)13s
    📚 Scrapped items: %(scrapped)13s
        """ % {
            'start_time': time.strftime('%H:%M:%S', time.localtime(self.bm_start_time)),
            'end_time': time.strftime('%H:%M:%S', time.localtime(self.bm_end_time)),
            'delta_h': dt_h,
            'delta_m': dt_m,
            'delta_s': dt_s,
            'crawled': str(self.bm_pages),
            'scrapped': str(self.bm_items),
        })

    def parse(self, response):
        """
        EDIT ME ! :>
        Parses the given response from the server.
        Modify to add your desired selectors, extractions, et cetera.
        """
        for task in [list(gen) for gen in [
            self.extract_images(response, 'img::attr(src)'),
            self.extract_files(response, 'a::attr(href)'),
            self.keep_crawling(response, 'a::attr(href)'),
        ]]:
            for item in task:
                yield item

    def keep_crawling(self, response, selector):
        """
        Continue navigating to the page referenced by the selected item.
        """
        for href in response.css(selector):
            href = href.extract()
            if not self.is_image(href) and not self.is_doc(href):
                if not self.limit_ or self.processed < self.limit_:
                    self.processed += 1
                    self.bm_pages += 1
                    yield response.follow(href, self.parse)

    def extract_files(self, response, selector):
        """
        Extracts files based on the given selector.
        """
        for href in response.css(selector):
            href = href.extract()
            if self.is_doc(href):
                self.bm_items += 1
                href = self.fix_relative(response, href)
                yield DocumentItem(file_urls=[href])

    def extract_images(self, response, selector):
        """
        Extracts images based on the given selector.
        """
        for src in response.css(selector):
            src = src.extract()
            if self.is_image(src):
                self.bm_items += 1
                src = self.fix_relative(response, src)
                yield ImageItem(image_urls=[src])

    def is_image(self, resource):
        """
        Tells whether a given resource identifier refers to an image or not.
        """
        for img in self.imgs_:
            if img in resource:
                return True
        return False

    def is_doc(self, resource):
        """
        Tells whether a given resource identifier refers to a document or not.
        """
        for doc in self.docs_:
            if doc in resource:
                return True
        return False

    def fix_relative(self, response, resource):
        """
        Fixes relative paths. If the given resource is relative, builds and returns the absolute one; otherwise, does nothing.
        """
        if ('http' not in resource) and ('//' not in resource):
            return response.urljoin(resource)
        return resource

